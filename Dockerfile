FROM nginx:stable-alpine

# Variabili necessarie

ENV DOCKER_FULL_STACK_SERVICE_NAME="localhost"
ENV DOCKER_SERVICE_PORT=8080
ENV SERVICE_HOST="localhost"
ENV KEYCLOACK_SERVER="localhost"
ENV ACCOUNTING_SERVICE_BASEURL="https://localhost"
ENV SUBREQUEST_OUTPUT_BUFFER_SIZE="8192k"


COPY src/nginx.conf /etc/nginx/nginx.conf
COPY src/config.js /etc/nginx/config.js
COPY src/nginx.default.conf.template /etc/nginx/templates/default.conf.template
COPY src/pep.js /etc/nginx/pep.js


