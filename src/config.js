export default { config };

var config = {
    "pep_credentials" : "{{ keycloak_pep_credentials }}",
    "debug": true,
    "accounting": {
        "scope":        "{{ authorized_scopes }}",
        "service_name": "{{ docker_stack_name }}",
        "host":         "{{ service_host }}"
    },
    "hosts": [
        {
            "host": ["{{ service_host }}"],
            "audience": "{{ keycloack_client_id }}",
            "allow-basic-auth": "{{ pep_allow_basic_auth }}",
            "paths": [
                {
                    "name": "{{ keycloack_client_resource_name }}",
                    "path": "^/?.*$",
                    "methods": [
                        {
                            "method": "GET"
                        },
                        {
                            "method": "POST"
                        }
                    ]
                }
            ]
        }
    ]
}